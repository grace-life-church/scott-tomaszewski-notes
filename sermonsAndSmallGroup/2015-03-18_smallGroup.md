# Wednesday Small Group | 2015-03-18
**Rhul, Felicia, John**

>#### Ephesians 6:10-13
>New International Version (NIV)
>
>**10** Finally, be strong in the Lord and in his mighty power. **11** Put on the full armor of God, so that you can take your stand against the devil’s schemes. **12** For our struggle is not against flesh and blood, but against the rulers, against the authorities, against the powers of this dark world and against the spiritual forces of evil in the heavenly realms. **13** Therefore put on the full armor of God, so that when the day of evil comes, you may be able to stand your ground, and after you have done everything, to stand.

* "Stand firm" - not advancing, but also not retreating.  Why?
  * Jesus has already won, we just need to hold our victory
  * "Stand strong/firm **in the Lord**" - on the rock of Christ
    * Don't go fight alone, our victory is in Jesus

---

***What is spiritual warfare?***

* The devil's schemes - temptation and accusation

---

***Can you experience temptation and accusation without spiritual warfare?***

* Yes - While playing board game, you set a trap for another player
* Yes - Deception in sports

---

***How is spiritual warfare different from other forms of temptation and accusation?***

* The devil's schemes are temptation and accusation towards sin

---

***Can you experience temptation and accusation towards sin that isn't caused by the devil?  Can mankind commit temptation and accusation without the devil's schemes?***

* Its not immediately clear from the Bible.  Its possible that we can tempt and accuse our of our own free will and sin
  * In the beginning, the he Devil tempted us which led to the fall of man and brought sin into mankind.  So, the root of our sin is caused by the devil
* Ultimately, it doesn't really matter.  There exists temptation and accusation, and we must resist it

>#### Romans 7:15-24
>New International Version (NIV)
>
>**15** I do not understand what I do. For what I want to do I do not do, but what I hate I do. **16** And if I do what I do not want to do, I agree that the law is good. **17** As it is, it is no longer I myself who do it, but it is sin living in me. **18** For I know that good itself does not dwell in me, that is, in my sinful nature. For I have the desire to do what is good, but I cannot carry it out. **19** For I do not do the good I want to do, but the evil I do not want to do—this I keep on doing. **20** Now if I do what I do not want to do, it is no longer I who do it, but it is sin living in me that does it.
>
>**21** So I find this law at work: Although I want to do good, evil is right there with me. **22** For in my inner being I delight in God’s law; **23** but I see another law at work in me, waging war against the law of my mind and making me a prisoner of the law of sin at work within me. **24** What a wretched man I am! Who will rescue me from this body that is subject to death?

---

***When experiencing difficulties or struggles, how do you know if its spiritual warfare or just struggles?***

* Spiritual warfare if it causes us to stumble towards sin and away from God
* The Holy Spirit helps us with discernment
  * Often, after the occurrence, we can tell if we were led to sin

---

***Is there a way to know spiritual warfare before it occurs or during?***

>#### Ephesians 4:26-27
>New International Version (NIV)
>
>**26** “In your anger do not sin”: Do not let the sun go down while you are still angry, **27** and do not give the devil a foothold.

* Know your weaknesses and don't give the Satan a foothold

>#### John 10:4-5
>New International Version (NIV)
>
>**4** When he has brought out all his own, he goes on ahead of them, and his sheep follow him because they know his voice. **5** But they will never follow a stranger; in fact, they will run away from him because they do not recognize a stranger’s voice.”

* By knowing the voice of God and what isn't the voice of God, one can discern lies and identify weaknesses
  * Voice of God == Character of God
  * To know the voice and character of God, remember foundations
    * prayer
    * wise council
    * scripture

When Satan uses scripture against us, it's not because the word is wrong.  Instead, we are misunderstanding God's character
* Words can be interpreted in many ways, but God's character never changes
* Always recall the character of God when reading scripture

---

### Summary

Our struggles are not always caused by spiritual warfare, but if we are led to sin, then the devil's schemes are upon us.  We must resist his temptations and accusations by identifying the devil's lies and our weaknesses through the word - God's voice and character.  To resist the devil, we must be strong in the Lord, Jesus Christ.


Also see:

* [Sunday sermon](2015-03-15_sermon.md)
* [Sunday small group](2015-03-15_smallGroup.md)
