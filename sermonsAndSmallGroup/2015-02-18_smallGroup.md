# Sunday Small Group | 2015-02-18
**John, Felicia, Rhul**

### Continuing Ephesians series

***Why are Gentiles different?  Why must we not do things like them?***

* Not sure we answered this one

Futility of thinking

* Selfish thinking
* Thinking without hope or in despair
* Temporal, non-eternal thinking

>#### Ephesians 4:19-24
>
> **19** and they, having become callous, have given themselves over to sensuality for the practice of every kind of impurity with greediness. **20** But you did not learn Christ in this way, **21** if indeed you have heard Him and have been taught in Him, just as truth is in Jesus, **22** that, in reference to your former manner of life, you lay aside the old self, which is being corrupted in accordance with the lusts of deceit, **23** and that you be renewed in the spirit of your mind, **24** and put on the new self, which in the likeness of God has been created in righteousness and holiness of the truth.

* Futility of thinking leads to lack of sensitivity and no longer caring
* Futility of thinking leads to not feeling the consequences of indulging impurity

***We are called to put aside old self and be right in God's eyes.  How do we do this?***
* Ephesians 4:23 says "to be renewed in the spirit of your mind"
* Think back to when we talked about speech, life, love, faith, and purity
  * [Sunday Service](2015-01-25_sundayService.md)
  * [Sunday Small Group](2015-01-25_smallGroup.md)
  * [Wednesday Small Group](2015-01-28_smallGroup.md)

***Why should we do this?***

>#### Ephesians 4:25
>
>**25** Therefore, laying aside falsehood, speak truth each one of you with his neighbor, for we are members of one another.

* We should put aside our old self and be right in God's eyes because we are one body
  * See 1 Corinthians 12 where its talks about gifts and a single body

***When we set aside our old selves and be renewed in the spirit of our mind, what do we become?***

>#### Ephesians 5:1-2
>
> **1** Therefore be imitators of God, as beloved children; **2** and walk in love, just as Christ also loved you and gave Himself up for us, an offering and a sacrifice to God as a fragrant aroma.

* We should become imitators of God.  In order to be a good imitator, we must *know* the thing we are imitating.
  * See [Sunday small group on being with God](2015-02-15_smallGroup.md)
* As God's children, we are loved by Him
* As our Father, God doesn't want us to ruin our lives with futility of thinking

***How do we become renewed in the spirit of our mind?***

>#### Philippians 2:12-13
>
> **12** So then, my beloved, just as you have always obeyed, not as in my presence only, but now much more in my absence, work out your salvation with fear and trembling; **13** for it is God who is at work in you, both to will and to work for His good pleasure.

* This allows us to be made new
* Be en empty vessels, remove expectations and agendas
* We should put ourselves in the best position possible to be made new

Also see:

* [Sunday Service](2015-02-15_sundayService.md)
* [Sunday Small Group](2015-02-15_smallGroup.md)
