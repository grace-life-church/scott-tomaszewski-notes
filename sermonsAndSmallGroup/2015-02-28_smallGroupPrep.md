#Sunday Small Group Preparation | 2015-02-28
**PR is giving sermon, I am Leading Small Group**

##Overview
>####Ephesians 5:1-20

God Calls us to live our lives as Imitators of Him as we live a Life of Love (5:1-2), which is seen in our relationships with others and in our speech (5:3-7).   We are to also live our lives as children of the Light and no longer walk in the dark with those who are still in the dark (5:8-14).  And lastly, we are called to live our lives in Wisdom making the most of every opportunity (15-20).
Once again, all these things are for those who are follower of Christ.  It is impossible for those who are still lost, in the dark, and in the world to try to live a life of Love which Christ did for very long if at all.  The question is not are we doing these things perfectly but are we striving, growing and progressing becoming more like Christ……  It really is impossible to live this life that Paul talks about and is exhorting the Ephesians to do if we have not experienced and are living with what Paul talked about in chapters 1-3.


###Some Possible Questions for Small Group:

####How do we get away from making all this into a rule keeping and religious duty?
  * Love Relationship with God that is Real and Fresh

####Why do so many of us struggle to obey/live our lives as we should?  And what can we do?
  * our sinful nature still lives, the world that is constantly tugging at us, the Devil whose job is to pull us down and away from God, etc
  * we need to follow verses like James 4:7

####Paul gives the Ephesians a firm warning in verses 5-6, what do you think these verses mean as they seem very harsh and final?  
  * warning against not those who are struggling or when we stumble but referring to those who have chosen to live out their life in this way even though they call themselves Christians.  It is for those who are practicing such lifestyle and not a momentary lapse or sin in their life

####Verse 7, talks about having nothing to do with those who live in such a way.  Then what about evangelism and being a witness in the world or the fact that everywhere we go we are surrounded by people who are in the dark like our co-workers and such?  

####How can we know if we are living as Children of Light or that we are failing?
  * Verse 9, the fruit of our lives.  What do you see in terms of fruit in your life

####Verses 15-20 – are you living your life in Wisdom, making the most of every opportunity?  What does making the most of every opportunity mean?  And what would your life look like if you did live your life in such away?
  * the point is that we are all living our life here on borrowed time, Life of Grace, another way to look at it is that we are all missionaries here, to be the “salt and the light to the world,” thus, it is clear what Paul is talking about in these verses which is that we are to make the most of every opportunity to reflect, reveal and share who God is with those around, and the Great Commission

##Preparation

* Read passage out loud, then to selves
* Thoughts, things that stood out?
* Summary
    * Live life as imitators (5:1-2)
    * Our imitation is seen in our relationships and speech (5:3-7)
    * Live life as children of the Light, dont walk in dark or with others in the dark (5:8-14)
    * Live lives in Wisdom, make most of every opportunity (5:15-20)
* Positive things are well established, what about the negative?

  >####Ephesians 5:5-6
  >
  >**5** For of this you can be sure: No immoral, impure or greedy person—such a person is an idolater—has any inheritance in the kingdom of Christ and of God. **6** Let no one deceive you with empty words, for because of such things God’s wrath comes on those who are disobedient.

    * ***What do you think these verses mean as they seem very harsh and final?***
    * Who is the intended audience of these verses?
      * Is this a warning against those who are struggling or those who stumble?
      * This is a warning for the people who claim to be Christians, but have chosen to live out their lives in this way
      * For those who are practicing this lifestyle and are continuously in sin

  >####Ephesians 5:7-8
  >
  >**7** Therefore do not be partners with them.  **8** For you were once darkness, but now you are light in the Lord. Live as children of light

    * Talks about having nothing to do with those who live in such a way.
    * ***What about evangelism and being a witness in the world?***

      >####Mark 16:15
      >
      >**15** And He said to them, “Go into all the world and preach the gospel to all creation.

      >####Matthew 28:19-20
      >
      >**19** Therefore go and make disciples of all nations, baptizing them in the name of the Father and of the Son and of the Holy Spirit, **20** and teaching them to obey everything I have commanded you. And surely I am with you always, to the very end of the age.”

    * ***Says dont be partners, yet we are surrounded by people who are in the dark - co-workers, family, friends***
    * When it seems there is a contradiction, look closer.
    * Says, "Do not be partners **with them**" - instead, help them be partners **with you**
    * Although it seems at first to be a verse against evangelism, it's actually the opposite.  
    * **Ephesians 5:8** is your justification for doing so
      * Evangelize because you were once in darkness too
