# Sunday Service | 2015-02-08
**Pastor Rogers**

> **[1 Corinthians 12:31](https://www.biblegateway.com/passage/?search=1+Corinthians+12%3A31&version=NIV)** (NIV)
>
> <sup>**31**</sup> Now eagerly desire the greater gifts.<br />
> And yet I will show you the most excellent way.

* Most important thing is love

---

> **[Matthew 28:19](https://www.biblegateway.com/passage/?search=matthew+28%3A19&version=NIV)** (NIV)
>
> <sup>**19**</sup> Therefore go and make disciples of all nations, baptizing them in the name of the Father and of the Son and of the Holy Spirit,

---

> **[Luke 10:2](https://www.biblegateway.com/passage/?search=luke+10%3A2&version=NIV)** (NIV)
>
> <sup>**2**</sup> He told them, “The harvest is plentiful, but the workers are few. Ask the Lord of the harvest, therefore, to send out workers into his harvest field.

* Lots, but not all hearts are ready

---

> **[John 4 ](https://www.biblegateway.com/passage/?search=john+4&version=NIV)** (NIV)

* Jesus and the samarian woman
* Jesus went to a place where there was a lot of hatred
* **Evangelism** - if you get tired, sit down and rest a minute
  * If you see someone, say something to start a conversation.  Not necessarily "Have you accepted Jesus."  Just talk.
  * Eventually turn the conversation into a spiritual topic

---

> **[Luke 9:5](https://www.biblegateway.com/passage/?search=luke+9%3A5&version=NIV)** (NIV)
>
> <sup>**5**</sup> If people do not welcome you, leave their town and shake the dust off your feet as a testimony against them.”

* Samarian woman has a large debate with Jesus
  * Rarely does a debate result in someone coming to Christ
* Sometimes it comes down to the simple truth
  * Often we don't have the answer
  * Lots of "hot topics" that are still uncertain

---

### Three things too look for

1. Sense a seeking heart
1. Generally interested
1. Receptive

* Next, talk about Jesus
* Sometimes you are just planting seeds
  * Samarian woman went back to town unsure
* Be so excited about Jesus that you can't help but go out to tell people

---

### Ministry and Evangelism

* In everyday life, are you around people?
  * Meet people
* As you meet people, say something
* Turn conversation into a spiritual one
* Then decide:
  * Convert them
  * Invite to church
  * Just plant seed and move on

---

***Share gospel - Help someone along in their walk***

---

> **[Genesis 11 ](https://www.biblegateway.com/passage/?search=genisis+11&version=NIV)** (NIV)

* Whole world had one language
* Men were going to build a city
* "Come let us go down and confuse their language so they will not understand each other"
* If we have some focus, we can do anything

***What is our common focus?***

Also see:

* [Church retreat - Friday](2105-02-06_churchRetreat.md)
* [Church retreat - Saturday](2105-02-07_churchRetreat.md)
