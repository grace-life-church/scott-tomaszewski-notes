# Sunday Small Group | 2015-03-22
**Mike McQuitty**

***After considering spiritual warfare from the past two weeks, how do you think the past week has been?***

---

Sometimes difficulties are spiritual warfare, sometimes its just life

***When struggling, how do you distinguish between spiritual warfare and just life difficulties?***

* Although an event isn't warfare, sometimes the temptations and accusations that come with it *are* warfare
* It's important to see that every event isn't spiritual warfare
* At the same time, don't ignore spiritual warfare
* See [first part of today's sermon](2015-03-22_sermon.md)

---

***Why doesn't the Bible talk more about demons?***

* Bible gives us the solution, we don't need to know more
* If we have some of the secrets to overcoming the enemy, we would no longer depend on the Lord

---

Righteousness comes from Christ

* When in your sin, Christ's righteousness is strong

---

***What do you fear regarding spiritual warfare?***

***How do you get beyond these fears?***

>#### Ephesians 6:10 New International Version (NIV)
>
>**10** Finally, be strong in the Lord and in his mighty power

* Be strong in the Lord

>#### Ephesians 6:18
>New International Version (NIV)
>
>**18** And pray in the Spirit on all occasions with all kinds of prayers and requests. With this in mind, be alert and always keep on praying for all the Lord’s people.

* Pray for one another for all things
