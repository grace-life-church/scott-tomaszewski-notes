# Sunday Small Group | 2015-01-25
**Mike McQuitty**

>####Titus 1:10-11
>
>**10** For there are many rebellious people, full of meaningless talk and deception, especially those of the circumcision group. **11** They must be silenced, because they are disrupting whole households by teaching things they ought not to teach—and that for the sake of dishonest gain.

* Avoid fads from divisive people
* Fads are distractions in most cases from people who are warped

>####Titus 3:9-11
>
>**9** But avoid foolish controversies and genealogies and arguments and quarrels about the law, because these are unprofitable and useless. 10 Warn a divisive person once, and then warn them a second time. After that, have nothing to do with them. 11 You may be sure that such people are warped and sinful; they are self-condemned.

* Don't be argumentative and quarrelsome

>#### 1 Timothy 4:15-16
>
>**15** Be diligent in these matters; give yourself wholly to them, so that everyone may see your progress. **16** Watch your life and doctrine closely. Persevere in them, because if you do, you will save both yourself and your hearers.

* These are the key points (see sermon on 2015-01-25)
* Its important to see others grow because it challenges and encourages them

---

Mike McQuitty's advise: Make a routine of doing something greater at least once per year.

Also see:

* [Sunday Sermon](2015-01-25_sermon.md)
* [Wednesday Small Group](2015-01-28_smallGroup.md)

##Notes

>####Titus 1:15-16
>
>**15** To the pure, all things are pure, but to those who are corrupted and do not believe, nothing is pure. In fact, both their minds and consciences are corrupted. **16** They claim to know God, but by their actions they deny him. They are detestable, disobedient and unfit for doing anything good.
