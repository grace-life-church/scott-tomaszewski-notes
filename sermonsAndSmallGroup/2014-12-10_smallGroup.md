# Small Group | 2014-12-10
**John, Felicia, Rhul**

>#### Ephesians 4:1
>
> **1** As a prisoner for the Lord, then, I urge you to live a life worthy of the calling you have received.

***Is your life worthy of God's call?  Worthy of Jesus' sacrifice on the cross?***

***What eternal impact have you made on those around you?***

NASB translation says, "walk in a manner worthy of the calling with which you have been called"

Walk with Christ

* moment-by-moment
* even a few steps might look bad, but you need to look at whole path
  * Opposite is true too! Even if you take a few good steps, but your overall path is bad, you are not walking with Christ
* Casual stroll
  * Great things = skip
  * Bad things = run
  * Stroll = do you go to God in your strolls?  ...on your relaxing days?
  * Don't simply go to Christ with great or bad things.  Christ desires a relationship with you more than anything.
    * See Discipleship part 2

***Why is it hard to live the life we are called to?***

* We are so detached from God in daily life

***What is one practical thing you can do to get closer to living that life?***

* More intentional prayer
* Discipleship part 2, session 2

Also see:

* [Sunday Service](2014-12-07_sermon.md)
* [Small Group Discussion](2014-12-17_smallGroup.md)
