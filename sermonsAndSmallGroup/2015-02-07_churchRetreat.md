# Church Retreat - Saturday | 2015-02-07
**Pastor Rogers**

## Morning Session

> **[1 Corinthians 12 ](https://www.biblegateway.com/passage/?search=1%20Corinthians%2012)** (NIV)
>
> 12&nbsp;Now about the gifts of the Spirit, brothers and sisters, I do not want you to be uninformed. <sup>**2**</sup> You know that when you were pagans, somehow or other you were influenced and led astray to mute idols. <sup>**3**</sup> Therefore I want you to know that no one who is speaking by the Spirit of God says, “Jesus be cursed,” and no one can say, “Jesus is Lord,” except by the Holy Spirit.<br />
> <sup>**4**</sup> There are different kinds of gifts, but the same Spirit distributes them. <sup>**5**</sup> There are different kinds of service, but the same Lord. <sup>**6**</sup> There are different kinds of working, but in all of them and in everyone it is the same God at work.<br />
> <sup>**7**</sup> Now to each one the manifestation of the Spirit is given for the common good. <sup>**8**</sup> To one there is given through the Spirit a message of wisdom, to another a message of knowledge by means of the same Spirit, <sup>**9**</sup> to another faith by the same Spirit, to another gifts of healing by that one Spirit, <sup>**10**</sup> to another miraculous powers, to another prophecy, to another distinguishing between spirits, to another speaking in different kinds of tongues, and to still another the interpretation of tongues. <sup>**11**</sup> All these are the work of one and the same Spirit, and he distributes them to each one, just as he determines.<br />
> <sup>**12**</sup> Just as a body, though one, has many parts, but all its many parts form one body, so it is with Christ. <sup>**13**</sup> For we were all baptized by one Spirit so as to form one body—whether Jews or Gentiles, slave or free —and we were all given the one Spirit to drink. <sup>**14**</sup> Even so the body is not made up of one part but of many.<br />
> <sup>**15**</sup> Now if the foot should say, “Because I am not a hand, I do not belong to the body,” it would not for that reason stop being part of the body. <sup>**16**</sup> And if the ear should say, “Because I am not an eye, I do not belong to the body,” it would not for that reason stop being part of the body. <sup>**17**</sup> If the whole body were an eye, where would the sense of hearing be? If the whole body were an ear, where would the sense of smell be? <sup>**18**</sup> But in fact God has placed the parts in the body, every one of them, just as he wanted them to be. <sup>**19**</sup> If they were all one part, where would the body be? <sup>**20**</sup> As it is, there are many parts, but one body.<br />
> <sup>**21**</sup> The eye cannot say to the hand, “I don’t need you!” And the head cannot say to the feet, “I don’t need you!” <sup>**22**</sup> On the contrary, those parts of the body that seem to be weaker are indispensable, <sup>**23**</sup> and the parts that we think are less honorable we treat with special honor. And the parts that are unpresentable are treated with special modesty, <sup>**24**</sup> while our presentable parts need no special treatment. But God has put the body together, giving greater honor to the parts that lacked it, <sup>**25**</sup> so that there should be no division in the body, but that its parts should have equal concern for each other. <sup>**26**</sup> If one part suffers, every part suffers with it; if one part is honored, every part rejoices with it.<br />
> <sup>**27**</sup> Now you are the body of Christ, and each one of you is a part of it. <sup>**28**</sup> And God has placed in the church first of all apostles, second prophets, third teachers, then miracles, then gifts of healing, of helping, of guidance, and of different kinds of tongues. <sup>**29**</sup> Are all apostles? Are all prophets? Are all teachers? Do all work miracles? <sup>**30**</sup> Do all have gifts of healing? Do all speak in tongues? Do all interpret? <sup>**31**</sup> Now eagerly desire the greater gifts.<br />
> And yet I will show you the most excellent way.

* We all have different gifts, but the same spirit
* The Body is one unit, but it has many parts
* We are all baptized into the same spirit
* Every part is important
* If one part suffers, all parts suffer
* The church is horizontal, not vertical
  * No hierarchy in terms of gifts being superior
* We are rewarded based on faith, not "importance"
* Doing things outside your gifts at the expense of your gift's outcome is bad
  * Even if something is positive for the church, if it has a negative impact on the things God expects you to do, then you must not partake in it

> **[1 Corinthians 12:31](https://www.biblegateway.com/passage/?search=1+Corinthians+12%3A31&version=NIV)** (NIV)
>
> <sup>**31**</sup> Now eagerly desire the greater gifts.<br />
> And yet I will show you the most excellent way.

* Even if your passions don't align with your gift, you should step out and seek the greater gift

---

With each gift, there are certain things to be cautious of

* Awareness of these cautions will...
  * Help combat the cautions
  * Allow you to shine with your gifts in building up the church
* Cautions hinder your ability to be effective in your gift

---

## Evening Praise

> **[Acts 1:5](https://www.biblegateway.com/passage/?search=acts+1%3A5&version=NIV)** (NIV)
>
> <sup>**5**</sup> For John baptized with water, but in a few days you will be baptized with the Holy Spirit.”

* Last thing Jesus said on Earth - His last wish
  * For all of us to receive the Holy Spirit and be His witness all over the Earth

---

> **[Philippians 3:15](https://www.biblegateway.com/passage/?search=Philipians+3%3A15&version=NIV)** (NIV)
>
> <sup>**15**</sup> All of us, then, who are mature should take such a view of things. And if on some point you think differently, that too God will make clear to you.

* Let us therefore, as many are perfect (mature), have this attitude, and if in anything you have a different attitude, God will reveal that also to you.
  * This is about guiding others

  Also see:

  * [Church retreat - Friday](2105-02-06_churchRetreat.md)
  * [Church retreat - Sunday](2105-02-08_churchRetreat.md)
