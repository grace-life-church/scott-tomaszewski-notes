# Ephesians Series 2014-2015
***Grace Life Church***

## Chapter 1-3 Summary
Chapters 1-3 discuss the spiritual privileges of the church, spelling out the great Christian truths of experience and belief

- [Ephesians 1:3-14 - Praise for Spiritual Blessings in Christ](#ephesians-13-14-praise-for-spiritual-blessings-in-christ)
- [Ephesians 1:15-23 - Paul's Prayer Requests, Thanksgiving and Prayer](#ephesians-115-23-pauls-prayer-requests-thanksgiving-and-prayer)
- [Ephesians 2:1-10 - From Death to Life, Made alive in Christ](#ephesians-21-10-from-death-to-life-made-alive-in-christ)
- [Ephesians 2:11-22 - Unity in Christ](#ephesians-211-22-unity-in-christ)
- [Ephesians 3:1-13 The Mystery has been Revealed, God's Marvelous Plan](#ephesians-31-13-the-mystery-has-been-revealed-gods-marvelous-plan)
- [Ephesians 3:14-21 - Paul's Second Prayer](#ephesians-314-21-pauls-second-prayer)

>#### Ephesians 1:3-14 - Praise for Spiritual Blessings in Christ
>
> **3** Praise be to the God and Father of our Lord Jesus Christ, who has blessed us in the heavenly realms with every spiritual blessing in Christ. **4** For he chose us in him before the creation of the world to be holy and blameless in his sight. In love **5** he predestined us for adoption to sonship through Jesus Christ, in accordance with his pleasure and will— **6** to the praise of his glorious grace, which he has freely given us in the One he loves. **7** In him we have redemption through his blood, the forgiveness of sins, in accordance with the riches of God’s grace **8** that he lavished on us. With all wisdom and understanding, **9** he made known to us the mystery of his will according to his good pleasure, which he purposed in Christ, **10** to be put into effect when the times reach their fulfillment—to bring unity to all things in heaven and on earth under Christ.

> **11** In him we were also chosen, having been predestined according to the plan of him who works out everything in conformity with the purpose of his will, **12** in order that we, who were the first to put our hope in Christ, might be for the praise of his glory. **13** And you also were included in Christ when you heard the message of truth, the gospel of your salvation. When you believed, you were marked in him with a seal, the promised Holy Spirit, **14** who is a deposit guaranteeing our inheritance until the redemption of those who are God’s possession—to the praise of his glory.

---

>#### Ephesians 1:15-23 - Paul's Prayer Requests, Thanksgiving and Prayer
>
> **15** For this reason, ever since I heard about your faith in the Lord Jesus and your love for all God’s people, **16** I have not stopped giving thanks for you, remembering you in my prayers. **17** I keep asking that the God of our Lord Jesus Christ, the glorious Father, may give you the Spirit of wisdom and revelation, so that you may know him better. **18** I pray that the eyes of your heart may be enlightened in order that you may know the hope to which he has called you, the riches of his glorious inheritance in his holy people, **19** and his incomparably great power for us who believe. That power is the same as the mighty strength **20** he exerted when he raised Christ from the dead and seated him at his right hand in the heavenly realms, **21** far above all rule and authority, power and dominion, and every name that is invoked, not only in the present age but also in the one to come. **22** And God placed all things under his feet and appointed him to be head over everything for the church, **23** which is his body, the fullness of him who fills everything in every way.

---

>#### Ephesians 2:1-10 - From Death to Life, Made alive in Christ
>
> **1** As for you, you were dead in your transgressions and sins, **2** in which you used to live when you followed the ways of this world and of the ruler of the kingdom of the air, the spirit who is now at work in those who are disobedient. **3** All of us also lived among them at one time, gratifying the cravings of our flesh and following its desires and thoughts. Like the rest, we were by nature deserving of wrath. **4** But because of his great love for us, God, who is rich in mercy, **5** made us alive with Christ even when we were dead in transgressions—it is by grace you have been saved. **6** And God raised us up with Christ and seated us with him in the heavenly realms in Christ Jesus, **7** in order that in the coming ages he might show the incomparable riches of his grace, expressed in his kindness to us in Christ Jesus. **8** For it is by grace you have been saved, through faith—and this is not from yourselves, it is the gift of God— **9** not by works, so that no one can boast. **10** For we are God’s handiwork, created in Christ Jesus to do good works, which God prepared in advance for us to do.

---

>#### Ephesians 2:11-22 - Unity in Christ
>
>**11** Therefore, remember that formerly you who are Gentiles by birth and called “uncircumcised” by those who call themselves “the circumcision” (which is done in the body by human hands)— **12** remember that at that time you were separate from Christ, excluded from citizenship in Israel and foreigners to the covenants of the promise, without hope and without God in the world. **13** But now in Christ Jesus you who once were far away have been brought near by the blood of Christ.
>
>**14** For he himself is our peace, who has made the two groups one and has destroyed the barrier, the dividing wall of hostility, **15** by setting aside in his flesh the law with its commands and regulations. His purpose was to create in himself one new humanity out of the two, thus making peace, **16** and in one body to reconcile both of them to God through the cross, by which he put to death their hostility. **17** He came and preached peace to you who were far away and peace to those who were near. **18** For through him we both have access to the Father by one Spirit.
>
>**19** Consequently, you are no longer foreigners and strangers, but fellow citizens with God’s people and also members of his household, **20** built on the foundation of the apostles and prophets, with Christ Jesus himself as the chief cornerstone. **21** In him the whole building is joined together and rises to become a holy temple in the Lord. **22** And in him you too are being built together to become a dwelling in which God lives by his Spirit.

---

>#### Ephesians 3:1-13 The Mystery has been Revealed, God's Marvelous Plan
>
>**1** For this reason I, Paul, the prisoner of Christ Jesus for the sake of you Gentiles—
>
>**2** Surely you have heard about the administration of God’s grace that was given to me for you, **3** that is, the mystery made known to me by revelation, as I have already written briefly. **4** In reading this, then, you will be able to understand my insight into the mystery of Christ, **5** which was not made known to people in other generations as it has now been revealed by the Spirit to God’s holy apostles and prophets. **6** This mystery is that through the gospel the Gentiles are heirs together with Israel, members together of one body, and sharers together in the promise in Christ Jesus.
>
>**7** I became a servant of this gospel by the gift of God’s grace given me through the working of his power. **8** Although I am less than the least of all the Lord’s people, this grace was given me: to preach to the Gentiles the boundless riches of Christ, **9** and to make plain to everyone the administration of this mystery, which for ages past was kept hidden in God, who created all things. **10** His intent was that now, through the church, the manifold wisdom of God should be made known to the rulers and authorities in the heavenly realms, **11** according to his eternal purpose that he accomplished in Christ Jesus our Lord. **12** In him and through faith in him we may approach God with freedom and confidence. **13** I ask you, therefore, not to be discouraged because of my sufferings for you, which are your glory.

---

>#### Ephesians 3:14-21 - Paul's Second Prayer
>
>**14** For this reason I kneel before the Father, **15** from whom every family in heaven and on earth derives its name. **16** I pray that out of his glorious riches he may strengthen you with power through his Spirit in your inner being, **17** so that Christ may dwell in your hearts through faith. And I pray that you, being rooted and established in love, **18** may have power, together with all the Lord’s holy people, to grasp how wide and long and high and deep is the love of Christ, **19** and to know this love that surpasses knowledge—that you may be filled to the measure of all the fullness of God.
>
>**20** Now to him who is able to do immeasurably more than all we ask or imagine, according to his power that is at work within us, **21** to him be glory in the church and in Christ Jesus throughout all generations, for ever and ever! Amen.

Also see

* [Small group discussion](2014-12-03_smallGroup.md) (also my first journal ever!)

## Chapter 4-6 Summary

Chapters 4-6 present the responsibilities of believers in their Christian walk in light of the truths of the first three chapters

>#### Ephesians 4:1-6 - Living a Worthy Life
>
> **1** As a prisoner for the Lord, then, I urge you to live a life worthy of the calling you have received. **2** Be completely humble and gentle; be patient, bearing with one another in love. **3** Make every effort to keep the unity of the Spirit through the bond of peace. **4** There is one body and one Spirit, just as you were called to one hope when you were called; **5** one Lord, one faith, one baptism; **6** one God and Father of all, who is over all and through all and in all.

Also see

* [Sunday Advent Service](2014-12-07_sermon.md)
* [Small Group Discussion](2014-12-10_smallGroup.md)
* [Small Group Discussion](2014-12-17_smallGroup.md)

---

>#### Ephesians 4:7-16 - Worthy Life Together
>
>**7** But to each one of us grace has been given as Christ apportioned it. **8** This is why it says:
>
>    “When he ascended on high,
>    he took many captives
>    and gave gifts to his people.”**
>
>**9** (What does “he ascended” mean except that he also descended to the lower, earthly regions? **10** He who descended is the very one who ascended higher than all the heavens, in order to fill the whole universe.) **11** So Christ himself gave the apostles, the prophets, the evangelists, the pastors and teachers, **12** to equip his people for works of service, so that the body of Christ may be built up **13** until we all reach unity in the faith and in the knowledge of the Son of God and become mature, attaining to the whole measure of the fullness of Christ.
>
>**14** Then we will no longer be infants, tossed back and forth by the waves, and blown here and there by every wind of teaching and by the cunning and craftiness of people in their deceitful scheming. **15** Instead, speaking the truth in love, we will grow to become in every respect the mature body of him who is the head, that is, Christ. **16** From him the whole body, joined and held together by every supporting ligament, grows and builds itself up in love, as each part does its work.

Also see

* [Small Group Discussion](2014-12-17_smallGroup.md)
