#Sunday Service | 2015-02-15
***Pastor Roger***

###Return of the Ephesians series - refreshing faith

> ####2 Corinthians 5:17
>
> Therefore, if anyone is in Christ, the new creation has come. The old has gone, the new is here!

* New in Christ

> ####Ephesians 4:17
>
> So I tell you this, and insist on it in the Lord, that you must no longer live as the Gentiles do, in the futility of their thinking.

* They are ignorant because of the hardening of their hearts
* Instead of criticizing, Paul chooses otherwise in the letter to Ephesians and is encouraging.  'You're doing it; keep on doing it!'

PR mentioned something about the Law of diminishing returns, but not sure what he was referencing

> ####Ephesians 4:19-20
>
> Having lost all sensitivity, they have given themselves over to sensuality so as to indulge in every kind of impurity, and they are full of greed.  That, however, is not the way of life you learned.

* We don't have to live this way
* The remainder of Ephesians 4 continues with rules...
  * In anger, don't sin
  * Do not steal
  * Only speak things that build people up
  * etc...

***What is our motivation to follow these rules?***

* Because God loves us
* Also see [wednesday small group](2015-02-18_smallGroup.md)

Following these rules is a *process*.  The thing about processes is that it leads to *progress*

**Overall, the word keeps you from**

* Ruining your life
* Harming others
* Squandering through life

> ####Ephesians 5:1-2
>
> Follow God’s example, therefore, as dearly loved children and walk in the way of love, just as Christ loved us and gave himself up for us as a fragrant offering and sacrifice to God.

* Be imitators of God

##Promises of God the gives us hope

I don't know why, but PR put up a slide with the promises of God between Ephesians 4:17 and Ephesians 4:19

> ####Romans 8:28
> ####Philippians 1:6
> ####Isaiah 38:17
> ####Hebrews 13:5
> ####Ephesians 1:5
> ####2 Corinthians 9:8
> ####Ephesians 1:8
> ####Ephesians 3:19

Also see:

* [Sunday Small Group](2015-02-15_smallGroup.md)
* [Wednesday Small Group](2015-02-18_smallGroup.md)
