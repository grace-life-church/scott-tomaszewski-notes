# Small Group | 2015-01-14
**John and Felicia Yahiro**

In Sermon, Phil went over Kings - King Joash

>#### 2 Chronicles 22-24

Just like you need to exercise in a way you enjoy (rock climbing), you must also pick a way to get closer to God in a way you enjoy

>#### Hebrews 5:11-14 - Warning Against Falling Away
>
>**11** We have much to say about this, but it is hard to make it clear to you because you no longer try to understand. **12** In fact, though by this time you ought to be teachers, you need someone to teach you the elementary truths of God’s word all over again. You need milk, not solid food! **13** Anyone who lives on milk, being still an infant, is not acquainted with the teaching about righteousness. **14** But solid food is for the mature, who by constant use have trained themselves to distinguish good from evil.

* Gospel can be in milk or solid food form
    * Milk = guidance from others, "pre-digested"
    * Solid food = seek on own, "digest yourself"
* Milk implies dependency - If the dependency (the provider of milk) goes away, solid food is going to be a struggle
* Try different things
* Always try to progress
    * If you always climb 5.11's, you will never climb a 5.12
    * Encourage others to progress
* Have a plan and stick to it
    * Be consistent
    * Be held responsible

### Summary of King Joash

* Joash has an evil Grandmother Athaliah who becomes Queen.  Other family members are also evil
* Joash's Aunt Jehosheba hides him in the temple for six years, then Joash becomes King
    * While in temple, his Uncle was priest and taught him the ways of the Lord
* Joash restores the temple
* Joash wants people (Levites) to collect money
* After Joash's Uncle died, Joash sought advice from others
    * Joash only lived on milk and was unable to sustain relationship when forced to switch to solid food
    * As the Uncle, you *must* let your child go.  You must let them think for themselves

***Lesson:*** Learn to think for yourself and digest on your own

*Why is the temple important?*

* Temple is where God is

*What does it mean to bring God to our workplace?*
