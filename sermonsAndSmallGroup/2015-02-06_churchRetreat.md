# Church Retreat - Friday | 2015-02-06
**Pastor Rogers**

###Opening

* Church is growing, but be careful that we don't lose community
* Throughout the Bible, God is trying to bring together community
  * Perfect community was Adam, Eve, and God

>#### Genesis 1:26-28
>New International Version (NIV)
>
>**26** Then God said, “Let us make mankind in our image, in our likeness, so that they may rule over the fish in the sea and the birds in the sky, over the livestock and all the wild animals, and over all the creatures that move along the ground.”
>
>**27** So God created mankind in his own image,  
>in the image of God he created them;  
>male and female he created them.
>
>**28** God blessed them and said to them, “Be fruitful and increase in number; fill the earth and subdue it. Rule over the fish in the sea and the birds in the sky and over every living creature that moves on the ground.”

* We are the perfect community

---

***What is most important?***

>#### Matthew 22:36-40
>New International Version (NIV)
>
>**36** “Teacher, which is the greatest commandment in the Law?”
>
>**37** Jesus replied: “‘Love the Lord your God with all your heart and with all your soul and with all your mind.’ **38** This is the first and greatest commandment. **39** And the second is like it: ‘Love your neighbor as yourself.’ **40** All the Law and the Prophets hang on these two commandments.”

* Love God
* Love your neighbor

---

#### Community needs

* Goals
  * Without, things become boring
* Growth
* Perspectives (many)
  * This is one reason why there are multiple gifts
* Grace
  * Communication breaks down sometimes.  Apologize and be gracious

#### Problems within communities

* Expectations
  * Kills community
  * Gives desire to surrender
  * Makes one less willing to open up
* Talking about work and accomplishments
  * Makes those that are not as fortunate feel uninvited, noninclusive

---

#### Community

* Don't give up
* Give it another chance
* Takes time, keep going

Also see:

* [Church retreat - Saturday](2105-02-07_churchRetreat.md)
* [Church retreat - Sunday](2105-02-08_churchRetreat.md)
