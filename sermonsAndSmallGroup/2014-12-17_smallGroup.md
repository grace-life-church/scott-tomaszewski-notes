# Small Group | 2014-12-17
**John, Felicia, Rhul**

>#### Ephesians 4:1
>
> **1** As a prisoner for the Lord, then, I urge you to live a life worthy of the calling you have received.

* Follow your calling

>#### Ephesians 4:2-3
>
>**2** Be completely humble and gentle; be patient, bearing with one another in love. **3** Make every effort to keep the unity of the Spirit through the bond of peace.

* unity

***What is a community with unity?***

* praying for someone
* missions
* small group

***How do you keep unity?*** - verse 2

* humility
* gentle
* patient
* show tolerance
* ...all in love

>#### Ephesians 4:11-13
>
>**11** So Christ himself gave the apostles, the prophets, the evangelists, the pastors and teachers, **12** to equip his people for works of service, so that the body of Christ may be built up **13** until we all reach unity in the faith and in the knowledge of the Son of God and become mature, attaining to the whole measure of the fullness of Christ.

***What differentiates a spiritual gift from something you do well?***

* Spiritual gifts
  * fruitful
  * they build the church up
  * Like parts of our physical bodies
    * they can be exercised
  * for building up the church, not for personal gain
    * See Ephesians 4:12
    * Paul says this so we know not to twist these gifts for our own good
* Not always important to distinguish spiritual gifts from natural talent
* God will sometimes use natural talent to build up the church
  * God uses our weaknesses to build us up
  * Moses + Aaron - Moses couldn't speak well, but he needed to lead people, so God provided him with Aaron

***What is the purpose of striving for community?***

* Its not simply to relieve loneliness or for the sake of being masters of community

>#### Ephesians 4:16
>
>**16** from whom the whole body, being fitted and held together by what every joint supplies, according to the proper working of each individual part, causes the growth of the body for the building up of itself in love.

* Community = body
* We need to grow the body to build it up
* We should grow it in love
* Purpose of community is to challenge others and so that we are challenged

Also see:

* [Sunday Service](2014-12-07_sermon.md)
* [Small Group Discussion](2014-12-10_smallGroup.md)
