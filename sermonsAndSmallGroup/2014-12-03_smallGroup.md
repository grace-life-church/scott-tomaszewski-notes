# Small Group | 2014-12-03

### Knowing God's Love

> #### Ephesians 3:14-21
> To know the love of Christ which surpasses knowledge

* To know something unknowable
* As much love as you can imagine, its **so** much more

---

***What's one way you have come to understand God's love better recently?***

Life has slowed down since getting married.  I appreciate the little things more.  Great grandparents, wife, coworkers, friends, relationships...

***Has it felt difficult to experience God's love?***

Its different; I am not used to listening for his love in this way.
