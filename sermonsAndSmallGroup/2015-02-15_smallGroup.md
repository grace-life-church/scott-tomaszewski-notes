#Sunday Small Group | 2015-02-15
***Mike McQuitty***

> ####John 1:35-39
>
> Again the next day John was standing with two of his disciples, and he looked at Jesus as He walked, and *said, “Behold, the Lamb of God!” The two disciples heard him speak, and they followed Jesus. And Jesus turned and saw them following, and *said to them, “What do you seek?” They said to Him, “Rabbi (which translated means Teacher), where are You staying?” He *said to them, “Come, and you will see.” So they came and saw where He was staying; and they stayed with Him that day, for it was about the tenth hour.

***What if we were asked, "What do you seek?"  What are our expectations?***

**The followers respond, "Where are you staying?"  Why was this the correct response?**

* They just want to be where Jesus is
* They aren't sure what to expect, they just want to be there

Sermon was about refreshing faith.  The best way to do this is to simply be with Jesus

> ####John 1:40-42
>
>One of the two who heard John speak and followed Him, was Andrew, Simon Peter’s brother. He found first his own brother Simon and said to him, “We have found the Messiah” (which translated means Christ). He brought him to Jesus. Jesus looked at him and said, “You are Simon the son of John; you shall be called Cephas” (which is translated Peter).

Andrew went to his brother Simon Peter to tell him he found Christ

* We are called to share!
* Andrew only spent the day with Jesus and he *knew* Jesus was the Messiah
* We need to spend time with Jesus

Peter is one of the leading disciples, Andrew wasn't one of the big ones

* BUT Andrew  brought Peter to Jesus
* We should always be bringing people to Jesus.  Even if we aren't a huge name, we might be a critical stepping stone.
* Andrew is always in the background bringing people to Jesus
* The 'Andrews' of the world are necessary!

> ####John 15:2
>
>He cuts off every branch in me that bears no fruit, while every branch that does bear fruit he prunes so that it will be even more fruitful.

* I believe we talked about this in regards to our gifts and how even if we have gifts that are fruitful, the Lord prunes those gifts in order to make them more fruitful

Also see:

* [Sunday Service](2015-02-15_sundayService.md)
* [Wednesday Small Group](2015-02-18_smallGroup.md)
