# Wednesday Small Group | 2015-02-11
**Rhul**

***How can we use our gifts and passions to go out and make disciples?***

---

## Gifts

#### Rhul

* Shepherding
* Teaching
* Intercession
* Task unstructured
* Equipping

#### Scott

* Helps
* Administration
* Word of knowledge
* Task unstructured
* Outreach (Supportive ministry/caring)

#### Peter

* Encouragement
* Helps
* Administration
* Task unstructured
* Equipping

#### John

* Giving
* Knowledge
* Discernment
* People/Task unstructured
* Outreach

#### Sam

* Creative communication
* Intercession
* Hospitality
* Task unstructured
* Celebration

#### Thomas

* Words of wisdom
* Mercy
* Faith
* People structured
* Caring (celebration)

#### Christina

* Craftsmanship
* Creative communication
* Mercy
* People unstructured
* Outreach/celebration

#### Felicia

* Faith
* Discernment
* People structured
* Equipping
