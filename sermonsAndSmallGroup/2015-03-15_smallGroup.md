# Sunday Small Group | 2015-03-15
**Scott Tomaszewski**

Sermon was on

>#### Ephesians 6:10-20 - The Armor of God
>
>**10** Finally, be strong in the Lord and in his mighty power. **11** Put on the full armor of God, so that you can take your stand against the devil’s schemes. **12** For our struggle is not against flesh and blood, but against the rulers, against the authorities, against the powers of this dark world and against the spiritual forces of evil in the heavenly realms. **13** Therefore put on the full armor of God, so that when the day of evil comes, you may be able to stand your ground, and after you have done everything, to stand. **14** Stand firm then, with the belt of truth buckled around your waist, with the breastplate of righteousness in place, **15** and with your feet fitted with the readiness that comes from the gospel of peace. **16** In addition to all this, take up the shield of faith, with which you can extinguish all the flaming arrows of the evil one. **17** Take the helmet of salvation and the sword of the Spirit, which is the word of God.
>
>**18** And pray in the Spirit on all occasions with all kinds of prayers and requests. With this in mind, be alert and always keep on praying for all the Lord’s people. **19** Pray also for me, that whenever I speak, words may be given me so that I will fearlessly make known the mystery of the gospel, **20** for which I am an ambassador in chains. Pray that I may declare it fearlessly, as I should.

**To win battles, God provides us with a set of armor.  What are the six pieces of The Armor of God?**

1. Belt of truth
1. Breastplate of righteousness
1. Feet fitted with gospel of peace
1. Shield of Faith
1. Helmet of salvation
1. Sword of the Spirit

***What is a specific, practical way to use each of these?***

***In what way is the sword of the Spirit, which is the word of God, an offensive weapon in contrast to the other defensive weapons?***

>#### Ephesians 5:26-27
>
>**26** to make her holy, cleansing her by the washing with water through the word, **27** and to present her to himself as a radiant church, without stain or wrinkle or any other blemish, but holy and blameless.

Through the word of the gospel, God accomplishes his powerful, cleansing work in the people's hearts and lives.

***Which piece of armor is difficult for you to utilize and why?  How can you learn to *actively* "take up" that weapon?***

***Ephesians 6:18-20 talks about prayer which can be classified as the final weapon.  How does prayer help us fight spiritual battles?***

#### Summary

Spiritual warfare is ever present in our lives, but we have a tools against it; we just need to actively take them up.  Over the next week, actively utilize the piece of armor you're weakest in.

Also see:

* [Sunday sermon](2015-03-15_sermon.md)
