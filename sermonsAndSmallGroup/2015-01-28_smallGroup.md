# Wednesday Small Group | 2015-01-28
**John Yahiro**

>#### 1 Timothy 4:12 New International Version (NIV)
>
>**12** Don’t let anyone look down on you because you are young, but set an example for the believers in speech, in conduct, in love, in faith and in purity.

* Normal reaction is to stand up for yourself, be rebellious
* Instead, scripture says to be an example

---

>#### Luke 6:45 New International Version (NIV)
>
>**45** A good man brings good things out of the good stored up in his heart, and an evil man brings evil things out of the evil stored up in his heart. For the mouth speaks what the heart is full of.

***What does it mean that the "mouth speaks what the heart is full of"?***

---

***What does it mean to be an example in in speech, in conduct, in love, in faith and in purity?***

---

Speech

* Whenever attacked, avoid natural reaction
* Maybe don't pray natural reaction away.  Instead look at root cause of *why* that is your natural reaction
  * Go back and think/pray through it
  * Love them

>#### Ephesians 4:29-32 New International Version (NIV)
>
>**29** Do not let any unwholesome talk come out of your mouths, but only what is helpful for building others up according to their needs, that it may benefit those who listen. **30** And do not grieve the Holy Spirit of God, with whom you were sealed for the day of redemption. **31** Get rid of all bitterness, rage and anger, brawling and slander, along with every form of malice. **32** Be kind and compassionate to one another, forgiving each other, just as in Christ God forgave you.

---

***When stretched thin, how do you be godly?***

* Remember that its by speech and actions that brought you to Christianity
* Little things like praying before meals, mentioning of church at work, etc

---

>#### 1 Timothy 4:12 New International Version (NIV)
>
>**12** Don’t let anyone look down on you because you are young, but set an example for the believers in speech, in conduct, in love, in faith and in purity.

* Preparing Timothy for his role
* We all must understand our role and accept it
* Understand the getting into your role involves making mistakes and God will give you challenges

>#### 1 Timothy 4:13 New International Version (NIV)
>
>**13** Until I come, devote yourself to the public reading of Scripture, to preaching and to teaching.

* those natural reactions are a sign of a true heart
* God changes our heart -> public reading of scripture, to exhortation, and to teaching

Also see:

* [Sunday Sermon](2015-01-25_sermon.md)
* [Sunday Small Group](2015-01-25_smallGroup.md)

### Summary

When we are attacked, we are called to "set an example in speech, in conduct, in love, in faith and in purity", but we may have a natural reaction to respond negatively instead.  This is a sign that are hearts are in the wrong place and instead of praying away the reaction, we should look at the heart.  A true heart, changed by God, will naturally react with good.
