# Small Group | 2015-01-07
**John and Felicia Yahiro**

### Overview of Hezekiah's rule as king

1. In the beginning, Hezekiah does good things and has great reforms
1. Becomes mortally sick and is told by prophet Isaiah that he will die.
1. On his death bed, he begs and prays to God for life
1. He is granted 15 more years of life
1. He shows off his riches and wealth to the other kings
1. Prophet tells Hezekiah that everything will perish including his children
1. Hezekiah doesn't care about the future, only cares for his lifetime

Also see: [Discipleship discussion on prayer](2015_03_03_discipleship.md)

* God is unchanging, but He responds to prayer

### King Hezekiah's Three Reforms

Happened at the beginning of his life/rule

1. Cleaned out the temple and opened its doors.  Said people should consecrate themselves and the temple.
2. Stationed Levites to praise the Lord
3. United people including his enemies

### After prayer on death bed

>#### 2 Kings 20

* Has a son, Manasseh, that was bad, little snot
    * Son took throne at age 12.  
* He raises his son terribly
* Prophet predicts doom, Hezekiah responds

>#### 2 Kings 20:19
>
>**19** “The word of the Lord you have spoken is good,” Hezekiah replied. For he thought, “Will there not be peace and security in my lifetime?”

* King Hezekiah was only concerned about his life, not about his legacy, descendants, future.
* He doesn't care about the prophet's warning about the future, he only cares about his lifetime

Also see

* [Sunday service](2015-01-04_sermon.md)
