# Sunday Small Group | 2015-01-18
**Mike McQuitty**

>####Jeremiah 33:17-18
>
>**17** For this is what the Lord says: ‘David will never fail to have a man to sit on the throne of Israel, **18** nor will the Levitical priests ever fail to have a man to stand before me continually to offer burnt offerings, to burn grain offerings and to present sacrifices.’”

* All of these kings, good and bad, are all begging the statement, "We need a perfect king"
* Overall: It looks like things are coming to an end, but I God, have a plan and that is to bring a perfect king - Jesus

**Key point**: No matter how far away things are, it can all be brought back to God.  (See Hezekiah's first month - people came back)

**Key point**: Everything you do affects the church

>####1 Peter 1:16
>
>**16** for it is written: “Be holy, because I am holy.”


>####1 Peter 1:22
>
>**22** Now that you have purified yourselves by obeying the truth so that you have sincere love for each other, love one another deeply, from the heart.

* You can live for God by loving others
* You can't be holy, love God, and not love your brother
* Your life affects others
* We are all a team

>####Ephesians 2:19-22
>
>**19** Consequently, you are no longer foreigners and strangers, but fellow citizens with God’s people and also members of his household, **20** built on the foundation of the apostles and prophets, with Christ Jesus himself as the chief cornerstone. **21** In him the whole building is joined together and rises to become a holy temple in the Lord. **22** And in him you too are being built together to become a dwelling in which God lives by his Spirit.

---

>####1 Timothy 4:12
>
>**12** Don’t let anyone look down on you because you are young, but set an example for the believers in speech, in conduct, in love, in faith and in purity.

Also see:

* [Sunday sermon](2015-01-18_sermon.md)
